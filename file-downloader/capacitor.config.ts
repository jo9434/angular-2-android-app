import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.myapp',
  appName: 'myapp',
  webDir: 'dist/file-download',
  bundledWebRuntime: false
};

export default config;
